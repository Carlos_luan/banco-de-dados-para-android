package com.example.myapplication;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class banco extends SQLiteOpenHelper {
    private static final int versao = 1;
    public banco (Context context)
    {
        super(context,"medico",null,versao);
    }
    public String inserir (String crm,String nome,String email)
    {
        SQLiteDatabase db = getWritableDatabase();
        String feedback;
        try
        {
            ContentValues values = new ContentValues();
            values.put("crm",crm);
            values.put("nome",nome);
            values.put("email",email);
            db.insert("medico",null,values);
            return "Contatos inseridos com sucesso";
        }catch (SQLException e)
        {
            feedback = e.getMessage().toString();
        }
        db.close();
        return feedback;
    }
    public String exibir ()
    {
        String banquinho="";
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT *FROM medico",null);
        if (cursor.moveToFirst()) {
            do {
                banquinho = banquinho + "\nCRM: " + cursor.getString(0) + "\nNome:" + cursor.getString(1) + "\nEmail:" + cursor.getString(2)+"\n---------------------------------------------------------";
            } while (cursor.moveToNext());
            db.close();
        }else {
            return "BANCO VAZIO";
        }
        return banquinho;
    }
    public void deletar (String crm)
    {
        SQLiteDatabase db = getWritableDatabase();
        db.delete("medico", "crm" + "=?", new String[]{String.valueOf(crm)});

    }
    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE IF NOT EXISTS medico (crm PRIMARY KEY, nome VARCHAR (45), email VARCHAR (45))");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
    db.execSQL("DROP TABLE IF EXISTS medico");
    onCreate(db);
    }

}
