package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
Button cadastrar,exibir,excluir;
TextView exibirtudo;
EditText crm,nome,email;
//necessário instanciar o banco de dados de forma global
banco db = new banco(this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        cadastrar= findViewById(R.id.cadastrarbtn);
        exibir = findViewById(R.id.exibirbtn);
        excluir= findViewById(R.id.excluirbtn);
        crm= findViewById(R.id.CRM);
        nome=findViewById(R.id.NOME);
        email=findViewById(R.id.EMAIL);
        exibirtudo=findViewById(R.id.textView3);
        cadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    String mensagem = db.inserir(crm.getText().toString(), nome.getText().toString(), email.getText().toString());
                    exibirtudo.setText(db.exibir());
                    Toast.makeText(getApplicationContext(), mensagem, Toast.LENGTH_LONG);
                }catch (Exception e)
                {
                    Toast.makeText(getApplicationContext(),e.getMessage(), Toast.LENGTH_LONG);
                }
            }
        });
    exibir.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
        exibirtudo.setText(db.exibir());
        }
    });
    excluir.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            try
            {
                db.deletar(crm.getText().toString());
                exibirtudo.setText(db.exibir());
                Toast.makeText(getApplicationContext(),"Registro excluido com sucesso!",Toast.LENGTH_SHORT);
            }catch (Exception e)
            {
                Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_LONG);
            }
            db.exibir();
        }
    });
    }
}
